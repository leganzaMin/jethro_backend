package com.sfc.legnaza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JethroBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(JethroBackendApplication.class, args);
	}

}
